package org.apache.servicecomb.fence.job;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import tech.powerjob.worker.core.processor.ProcessResult;
import tech.powerjob.worker.core.processor.TaskContext;
import tech.powerjob.worker.core.processor.TaskResult;
import tech.powerjob.worker.core.processor.sdk.BroadcastProcessor;
import tech.powerjob.worker.log.OmsLogger;

import java.util.List;

/**
 * PowerJob BroadcastProcessor  demo
 */
@Component
public class PowerJobBroadcastDemo implements BroadcastProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PowerJobBroadcastDemo.class);

    @Override
    public ProcessResult process(TaskContext taskContext) throws Exception {
        OmsLogger omsLogger = taskContext.getOmsLogger();
        Long jobId = taskContext.getJobId();
        omsLogger.info("Job:{} 前置启动", jobId);
        LOGGER.info("Job:{} 前置启动",jobId);
        return new ProcessResult(true, "init success");
    }

    @Override
    public ProcessResult preProcess(TaskContext context) throws Exception {
        OmsLogger omsLogger = context.getOmsLogger();
        Long jobId = context.getJobId();
        omsLogger.info("Job:{} 处理器处理中", jobId);
        LOGGER.info("Job:{} 处理器处理中", jobId);
        return new ProcessResult(true, "release resource success");
    }

    @Override
    public ProcessResult postProcess(TaskContext context, List<TaskResult> taskResults) throws Exception {
        OmsLogger omsLogger = context.getOmsLogger();
        Long jobId = context.getJobId();
        omsLogger.info("Job:{} 处理器处理完成", jobId);
        LOGGER.info("Job:{} 处理器处理完成", jobId);
        return new ProcessResult(true, "process success");
    }
}

